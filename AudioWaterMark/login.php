<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:46 PM
 */
include 'includes/message.php';
include "includes/init.php";

if(isset($_SESSION['username']))
{
    header('Location: index');
    exit();
}
?>
<html>
<head>
    <title>Login Page</title>
    <link rel="stylesheet" href="css/style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/customjs.js" type="text/javascript"></script>
</head>
<body>

<?php
if (isset($_GET['message_error'])&&$_GET['message_error']!=='')
{
    message_dialog(htmlspecialchars($_GET['message_error']), 'LỖI', 'Ok', 'challenge-attempt incorrect on-page-load');
}
elseif(isset($_GET['message'])&&$_GET['message']!=='')
{
    message_dialog(htmlspecialchars($_GET['message']), 'Thành công', 'OK!', 'challenge-attempt correct on-page-load');
}
?>
<div class="loginbox">
    <img src="images/avatar.png" alt="" class="avatar">
    <h1>ĐĂNG NHẬP</h1>
    <form action="action/login.php" method="post">
        <label>Tài khoản:</label>
        <input type="text" name="username" class="form-control" placeholder="Nhập tên tài khoản...">
        <label>Mật khẩu:</label>
        <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu..."><br/>
        <input type="hidden" value="login" name="action">
        <input type="submit" name="login" value="Đăng Nhập">
        <a class="button_register" data-toggle="modal" data-target="#register"><i class="fa fa-fw -square -circle fa-plus-square"></i> Đăng kí</a>
    </form>
</div>
<div class="fade modal" id="register">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myModalLabel">Đăng kí</h2>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="action/login.php" enctype="">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="username_reg">Tài khoản</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-pencil-square"></i>
                                        </span>
                                    <input id="username_reg" name="username_reg" class="form-control" placeholder="Nhập tên tài khoản.." type="text" required="">
                                </div>
                            </div>
                        </div>
                        <!-- password-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="password_reg">Mật khẩu</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-eye-slash"></i>
                                        </span>
                                    <input id="password_reg" name="password_reg" class="form-control" placeholder="Nhập mật khẩu.." type="password" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="password_reg2">Nhập lại mật khẩu</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-eye-slash"></i>
                                        </span>
                                    <input id="password_reg2" name="password_reg2" class="form-control" placeholder="Nhập  lại mật khẩu.." type="password" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">Email</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-eye-slash"></i>
                                        </span>
                                    <input id="email" name="email" class="form-control" placeholder="Nhập email..." type="email" required="">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="register" name="action">
                        <div class="form-group">
                            <button type="submit" class="col-md-2 btn btn-primary" style="margin-left: 215px;">
                                <i class="fa fa-fw fa-save"></i>Đăng kí</button>
                        </div>
                        <!-- Button -->
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>