<?php
/**
 * Created by PhpStorm.
 * User: PHANTOM0305
 * Date: 5/18/2018
 * Time: 12:45 AM
 */

include '../includes/database.php';
include '../includes/session.php';
include '../includes/init.php';

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

if(!isset($_SESSION['username']))
{
    header('Location: login');
    exit();
}

class WavFile{
    private static $HEADER_LENGTH = 44;

    public static function ReadFile($filename) {
        $filesize = filesize($filename);
        if ($filesize<self::$HEADER_LENGTH)
            return false;
        $handle = fopen($filename, 'rb');
        $wav = array(
            'header'    => array(
                'chunkid'       => self::readString($handle, 4),
                'chunksize'     => self::readLong($handle),
                'format'        => self::readString($handle, 4)
            ),
            'subchunk1' => array(
                'id'            => self::readString($handle, 4),
                'size'          => self::readLong($handle),
                'audioformat'   => self::readWord($handle),
                'numchannels'   => self::readWord($handle),
                'samplerate'    => self::readLong($handle),
                'byterate'      => self::readLong($handle),
                'blockalign'    => self::readWord($handle),
                'bitspersample' => self::readWord($handle)
            ),
            'subchunk2' => array( //INFO chunk is optional, but I need it for this project's audio file
                'id'            => self::readString($handle, 4),
                'size'			=> self::readLong($handle),
                'data'          => null
            ),
            'subchunk3' => array(
                'id'			=> null,
                'size'			=> null,
                'data'          => null
            )
        );
        $wav['subchunk2']['data'] = fread($handle, $wav['subchunk2']['size']);
        $wav['subchunk3']['id'] = self::readString($handle, 4);
        $wav['subchunk3']['size'] = self::readLong($handle);
        $wav['subchunk3']['data'] = fread($handle, $wav['subchunk3']['size']);
        fclose($handle);
        return $wav;
    }

    private static function readString($handle, $length) {
        return self::readUnpacked($handle, 'a*', $length);
    }

    private static function readLong($handle) {
        return self::readUnpacked($handle, 'V', 4);
    }

    private static function readWord($handle) {
        return self::readUnpacked($handle, 'v', 2);
    }

    private static function readUnpacked($handle, $type, $length) {
        $r = unpack($type, fread($handle, $length));
        return array_pop($r);
    }
}

try
{
    if ($_SERVER["REQUEST_METHOD"]==="POST")
    {
        if (isset($_POST['action'])&& $_POST['action']=="getSign")
        {
            if (isset($_FILES['filegetsign']))
            {
                $fileName = $_FILES["filegetsign"]["tmp_name"];
                $fileType = strtolower($_FILES['filegetsign']['type']);
                if ($fileType == "audio/wav") {

                    //Read audio file

                    $wavFile = new WavFile;
                    $tmp = $wavFile->ReadFile($fileName);
                    unlink($fileName);

                    //Get binary code of signature

                    function BintoText($bin)
                    {
                        $text = "";
                        for ($i = 0; $i < strlen($bin) / 8; $i++)
                            $text .= chr(bindec(substr($bin, $i * 8, 8)));
                        return $text;
                    }

                    $subchunk3data = unpack("H*", $tmp['subchunk3']['data']);

                    $signature = "";
                    for ($i = 0; $i < 80; $i++) {
                        $signature .= substr(str_pad(base_convert(substr($subchunk3data[1], $i * 2, 2), 16, 2), 8, '0', STR_PAD_LEFT), 7, 1);
                    }
                    $lenofsigndat = BintoText(substr($signature, 0, 80));
                    if (is_numeric($lenofsigndat)) {
                        for ($i = 80; $i < 80 + $lenofsigndat * 8; $i++) {
                            $signature .= substr(str_pad(base_convert(substr($subchunk3data[1], $i * 2, 2), 16, 2), 8, '0', STR_PAD_LEFT), 7, 1);
                        }
                        $signdat = BintoText(substr($signature, 80, $lenofsigndat * 8));
                    }
                }

                $message = $signdat;
                header("Location: ".__ROOT__."GetSignature?message=".$message);
                exit();
            }
        }
    }
}
catch (Exception $e)
{
    // return ERROR.
    $message_error = $signdat;
    header("Location: ".__ROOT__."GetSignature?message_error=".$message_error);
    exit();
}