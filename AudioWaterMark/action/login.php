<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:50 PM
 */
include '../includes/database.php';
include '../includes/session.php';
include '../includes/init.php';

function login($username, $password)
{
    $query ="SELECT * FROM users WHERE username = :username AND password = :password;";
    $values = array(":username"=>$username,
        ":password"=>$password);
    return db_query_fetch_one($query,$values);
}
function get_one_user($username)
{
    $query ="SELECT * FROM users WHERE username = :username;";
    $values = array(":username"=>$username);
    return db_query_fetch_one($query,$values);
}

function insert_user($username,$password,$email)
{
    $query = "INSERT INTO users VALUES(:username,:password,'no',:email);";
    $values = array(":username"=>$username,
        ":password"=>$password,
        ":email"=>$email);
    return db_query_fetch_none($query,$values);
}

function check_name($name)
{
    return preg_match("/[^a-zA-Z0-9@.]/",$name);
}

function insert_first_message($user_send, $title,$content_message,$user_receive)
{
    $query ="INSERT INTO send_message VALUES(null,:user_send,:title,:content_message,:user_receive);";
    $values = array(":user_send"=>$user_send,
        ":title"=>$title,
        ":content_message"=>$content_message,
        ":user_receive"=>$user_receive);
    db_query_fetch_none($query,$values);
}

if($_SERVER['REQUEST_METHOD']=="POST")
{
    if(isset($_POST['action'])&&$_POST['action']==='login')
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        if($username == ''|| $password =='')
        {
            $message_error = 'Chưa nhập tài khoản hoặc mật khẩu!!!';
            header("Location: ".__ROOT__.'login?message_error='.$message_error);
        }
        else
        {
            $password = sha1("s3cr3t".$password);
            $user = login($username,$password);
            if($user['username']=='')
            {
                $message_error = 'Tài khoản hoặc mật khẩu không đúng!!!';
                header("Location: ".__ROOT__.'login?message_error='.$message_error);
            }
            else
            {
                session_login_init($user);
                header("Location: ".__ROOT__);
            }
        }
    }
    elseif (isset($_POST['action'])&&$_POST['action']==='register')
    {
        if (!isset($_POST['username_reg'])||!isset($_POST['password_reg'])||!isset($_POST['password_reg2'])||!isset($_POST['email']))
        {
            $message_error = 'Điền thông tin không đầy đủ!!!';
            header("Location: ".__ROOT__.'login?message_error='.$message_error);
        }
        else
        {
            $username = $_POST['username_reg'];
            $password = $_POST['password_reg'];
            $re_password = $_POST['password_reg2'];
            $email = $_POST['email'];
            if(check_name($username)!=false||check_name($password)!=false||check_name($email)!=false)
            {
                $message_error = 'Tên không hợp lệ!!!';
                header("Location: ".__ROOT__.'login?message_error='.$message_error);
                exit();
            }
            $user = get_one_user($username);
            if($user['username']!='')
            {
                $message_error = 'Tài khoản đã tồn tại!!!';
                header("Location: ".__ROOT__.'login?message_error='.$message_error);
            }
            elseif ($password !== $re_password)
            {
                $message_error = 'Mật khẩu không trùng khớp!!!';
                header("Location: ".__ROOT__.'login?message_error='.$message_error);
            }
            else
            {
                $password = sha1("s3cr3t".$password);
                insert_user($username,$password,$email);
                $title_first_message = "Welcome to Noob Mail!!";
                $contents_message = "Hi, Have a nice day!!!";
                insert_first_message("admin",$title_first_message,$contents_message,$username);
                $message = 'Đăng kí thành công!!!';
                header("Location: ".__ROOT__.'login?message='.$message);
            }
        }
    }
}