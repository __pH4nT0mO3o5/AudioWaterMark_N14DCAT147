<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:46 PM
 */
include "includes/init.php";
include "includes/layout.php";
include "includes/database.php";
include "includes/message.php";

if(!isset($_SESSION['username']))
{
    header('Location: login');
    exit();
}
if($_SESSION['isadmin']==="yes")
    layout_header();
else
    layout_header_user();
if (isset($_GET['message_error'])&&$_GET['message_error']!=='')
{
    message_dialog(htmlspecialchars($_GET['message_error']), 'LỖI', 'Ok', 'challenge-attempt incorrect on-page-load');
}
elseif(isset($_GET['message'])&&$_GET['message']!=='')
{
    message_dialog(htmlspecialchars($_GET['message']), 'Thành công', 'OK!', 'challenge-attempt correct on-page-load');
}

function get_all_playlist()
{
    $query ="SELECT * FROM managerSong";
    return db_query_fetch_all($query);
}
function select_all_song()
{
    $query = "SELECT * FROM managerSong ORDER BY id DESC;";
    return db_query_fetch_all($query);
}
//function get_all_playlist_follow_username($username)
//{
//    $query = "SELECT managersong.id,managersong.fileUrl,managersong.name,managersong.fileId FROM managersong INNER JOIN managerlicense ON managersong.fileId=managerlicense.fileId AND managerlicense.username=:username;";
//    $values = array(":username"=>$username);
//    return db_query_fetch_all($query,$values);
//}
function get_all_playlist_follow_username($username)
{
    $query = "SELECT * FROM managerlicense WHERE username=:username";
    $values = array(":username"=>$username);
    return db_query_fetch_all($query,$values);
}
?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Danh Sách Bài Hát</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="<?php echo __ROOT__;?>">Danh Sách Bài Hát</a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!-- Bảng -->
                        <div class="section">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>id</b>
                                            </td>
                                            <td>
                                                <b>Tiêu đề</b>
                                            </td>
                                            <td>
                                                <b>Buy Song</b>
                                            </td>
                                        </tr>
                                        <?php
                                        $results = select_all_song();
                                        foreach ($results as $rows)
                                        {
                                            if($rows['name']!=='')
                                            {
                                                $content = '';
                                                $array = explode(" ",$rows['name']);
                                                if (str_word_count($rows['name']) >30)
                                                {
                                                    for($i =0;$i<30;$i++)
                                                    {
                                                        $nameSong= $nameSong.$array[$i].' ';
                                                    }
                                                    $nameSong=$nameSong.'....';
                                                }
                                                else
                                                    $nameSong = $rows['name'];
                                            }
                                            echo "
                                            <tr>
                                                <td>
                                                    <p>".htmlspecialchars($rows['id'])."</p>
                                                </td>                                               
                                                <td>
                                                    <p>".htmlspecialchars($nameSong)."</p>
                                                </td>
                                                <td>
                                                    <div class=\"btn-group\">
                                                    ";
                                            $results2 = get_all_playlist_follow_username($_SESSION['username']);
                                            $array = array();
                                            foreach ($results2 as $row2) {
                                                array_push($array, $row2["id"]);
                                            }
                                            if (in_array($rows["id"],$array))
                                            {
                                                echo "
                                                        <lable><b>LICENSED</b></lable>
                                                    </div>
                                                </td>
                                            </tr>
                                                ";
                                            }
                                            else
                                            echo "
                                                        <button id=\"".$rows['id']."\" class=\"btn btn-danger btn-mini btnbuysong\" ><i class=\"fa fa-shopping-cart\" ></i ></button >
                                                    </div>
                                                </td>
                                            </tr>                                        
                                            ";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sm2-bar-ui full-width fixed">
        <div class="bd sm2-main-controls">
            <div class="sm2-inline-texture"></div>
            <div class="sm2-inline-gradient"></div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#play" class="sm2-inline-button sm2-icon-play-pause">Play / pause</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-inline-status">
                <div class="sm2-playlist">
                    <div class="sm2-playlist-target">
                        <noscript><p>JavaScript is required.</p></noscript>
                    </div>
                </div>
                <div class="sm2-progress">
                    <div class="sm2-row">
                        <div class="sm2-inline-time">0:00</div>
                        <div class="sm2-progress-bd">
                            <div class="sm2-progress-track">
                                <div class="sm2-progress-bar"></div>
                                <div class="sm2-progress-ball"><div class="icon-overlay"></div></div>
                            </div>
                        </div>
                        <div class="sm2-inline-duration">0:00</div>
                    </div>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element sm2-volume">
                <div class="sm2-button-bd">
                    <span class="sm2-inline-button sm2-volume-control volume-shade"></span>
                    <a href="#volume" class="sm2-inline-button sm2-volume-control">volume</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#prev" title="Previous" class="sm2-inline-button sm2-icon-previous">&lt; previous</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#next" title="Next" class="sm2-inline-button sm2-icon-next">&gt; next</a>
                </div>
            </div>

            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#repeat" title="Repeat playlist" class="sm2-inline-button sm2-icon-repeat">&infin; repeat</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element sm2-menu">
                <div class="sm2-button-bd">
                    <a href="#menu" class="sm2-inline-button sm2-icon-menu">menu</a>
                </div>
            </div>
        </div>
        <div class="bd sm2-playlist-drawer sm2-element">
            <div class="sm2-inline-texture">
                <div class="sm2-box-shadow"></div>
            </div>
            <div class="sm2-playlist-wrapper">
                <ul class="sm2-playlist-bd">
                    <?php
                    if (isset($_SESSION['username'])){
                        if(isset($_SESSION['isadmin'])&& $_SESSION['isadmin']==='yes')
                        {
                            $rs_myplaylist = get_all_playlist();
                            foreach ($rs_myplaylist as $key => $value){
                                echo "<li>
											<div class=\"sm2-row\">
												<div class=\"sm2-col sm2-wide\">
													<a href=\"http://docs.google.com/uc?export=open&id=" . $value['fileId'] . "&type=.wav\"><b>" . $value['name'] . "</b><span class=\"label\">Licenced</span></a>
												</div>
												<div class=\"sm2-col\">
													<a href=\"http://docs.google.com/uc?export=open&id=" . $value['fileId'] . "\" target=\"_blank\" title=\"Download this song\" class=\"sm2-icon sm2-music sm2-exclude\">Download this song</a>
												</div>
											</div>
										</li>";
                            }
                        }
                        else
                        {
                            $rs_myplaylist = get_all_playlist_follow_username($_SESSION['username']);
                            foreach ($rs_myplaylist as $key => $value){
                                echo "<li>
											<div class=\"sm2-row\">
												<div class=\"sm2-col sm2-wide\">
													<a href=\"http://docs.google.com/uc?export=open&id=" . $value['fileNewId'] . "&type=.wav\"><b>" . $value['nameFile'] . "</b><span class=\"label\">Licenced</span></a>
												</div>
												<div class=\"sm2-col\">
													<a href=\"http://docs.google.com/uc?export=open&id=" . $value['fileNewId'] . "\" target=\"_blank\" title=\"Download this song\" class=\"sm2-icon sm2-music sm2-exclude\">Download this song</a>
												</div>
											</div>
										</li>";
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>


    <script type="text/javascript">
        $(".btnbuysong").click(function(){
            $("*").css("cursor", "wait");
            var id = $(this).attr("id");
            $.ajax({
                url: "action/buySong.php",
                type: "POST",
                data: { id : id },
                success : function(response){
                    $("*").css("cursor", "default");
                    if (response == "buy success"){
                        domain = window.location.pathname;
                        window.location= domain+"?message=Buy Success!";
                    }
                    else if (response == "buy failure"){
                        domain = window.location.pathname;
                        window.location= domain+"?message_error=Buy Failed!";
                        window.location="";
                    }
                }
            });
        });
    </script>
<?php
//layout_footer();
//?>