<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:17 PM
 */
session_start();
$currentCookieParams = session_get_cookie_params();
$sidvalue = session_id();
setcookie(
    'PHPSESSID',//name
    $sidvalue,//value
    0,//expires at end of session
    $currentCookieParams['path'],//path
    $currentCookieParams['domain'],//domain
    false, //secure
    true
);
define("__ROOT__","http://localhost:9696/AudioWaterMark/");
define("__tmpdir__","../../../tmp/music/");