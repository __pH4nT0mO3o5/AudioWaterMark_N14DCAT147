<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:51 PM
 */
function session_login_init($user)
{
    $_SESSION['username']=$user['username'];
    $_SESSION['email'] = $user['email'];
    $_SESSION['isadmin'] =$user['isadmin'];
}

function user_is_logged_in()
{
    return isset($_SESSION['username']);
}

function logout()
{
    session_unset();
    session_destroy();
    header("Location: ".__ROOT__."login");
}