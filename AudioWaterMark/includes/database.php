<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:20 PM
 */
define("HOST","localhost");
define("USERDB","root");
define("PASSDB","");
define("DBNAME","chall_ctf3");

$db = null;

#Get DB PDO
function get_db_pdo()
{
    global $db;

    if ($db === null)
    {
        try {
            $db = new PDO('mysql:host='.HOST.';dbname='.DBNAME.';charset=utf8', USERDB, PASSDB);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (Exception $e) {
            message_inline_red('Caught exception connecting to database');
            throw $e;
        }
    }
    return $db;
}

function db_query($query,array $values =null, $all = true)
{
    $db = get_db_pdo();

    try
    {
        if(!empty($values))
        {
            $stmt = $db->prepare($query);
            $stmt->execute($values);
        }
        else
        {
            $stmt = $db->query($query);
        }
        if ($all === true)
        {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        else
            return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e){

    }
}

function db_query_fetch_one($query, array $values =null)
{
    return db_query($query,$values,false);
}

function db_query_fetch_all($query, array $values=null)
{
    return db_query($query,$values,true);
}

function db_query_fetch_none($query, array $values=null)
{
    return db_query($query, $values, null);
}