<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 2:44 PM
 */
function message_inline_blue ($message, $strip_html = true) {
    echo '<div class="alert alert-info">',($strip_html ? htmlspecialchars($message) : $message),'</div>';
}
function message_inline_red ($message, $strip_html = true) {
    echo '<div class="alert alert-danger">',($strip_html ? htmlspecialchars($message) : $message),'</div>';
}
function message_inline_yellow ($message, $strip_html = true) {
    echo '<div class="alert alert-warning">',($strip_html ? htmlspecialchars($message) : $message),'</div>';
}
function message_inline_green ($message, $strip_html = true) {
    echo '<div class="alert alert-success">',($strip_html ? htmlspecialchars($message) : $message),'</div>';
}

function message_dialog ($message, $title, $closeText, $class) {

    echo '
    <div class="modal fade ',htmlspecialchars($class),'">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">',htmlspecialchars($title),'</h4>
                </div>
                <div class="modal-body">
                    <p>',htmlspecialchars($message),'</p>
                    <button type="button" class="btn btn-default" data-dismiss="modal">',htmlspecialchars($closeText),'</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    ';
}