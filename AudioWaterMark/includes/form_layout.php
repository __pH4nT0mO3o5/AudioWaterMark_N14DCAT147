<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/21/2018
 * Time: 2:34 PM
 */
function form_start($action='', $class='', $enctype='') {
    echo '
    <form method="post" class="',($class ? $class : 'form-horizontal'),'"',($enctype ? ' enctype="'.$enctype.'"' : ''),'',($action ? ' action="'.__ROOT__.$action.'"' : ''),' role="form">
    ';
    //form_xsrf_token();
}
function form_end() {
    echo '</form>';
}
function form_input_checkbox ($name, $checked = 0) {
    $name = htmlspecialchars($name);
    $field_name = strtolower(str_replace(' ','_',$name));
    echo '
    <div class="form-group">
      <label class="col-sm-2 control-label" for="',$field_name,'">',$name,'</label>
      <div class="col-sm-10">
          <input type="checkbox" id="',$field_name,'" name="',$field_name,'" value="1"',($checked ? ' checked="checked"' : ''),' />
      </div>
    </div>
    ';
}
function form_hidden ($name, $value) {
    $name = htmlspecialchars($name);
    $field_name = strtolower(str_replace(' ','_',$name));
    echo '<input type="hidden" name="',$field_name,'" value="',htmlspecialchars($value),'" />';
}

function form_button_submit ($name, $type = 'primary') {
    $name = htmlspecialchars($name);
    $field_name = strtolower(str_replace(' ','_',$name));
    echo '
    <div class="form-group">
      <label class="col-sm-2 control-label" for="',$field_name,'"></label>
      <div class="col-sm-10">
          <button type="submit" id="',$field_name,'" class="btn btn-',htmlspecialchars($type),'">',$name,'</button>
      </div>
    </div>
    ';
}

function section_subhead ($title, $tagline = '', $strip_html = true) {
    echo '
    <div class="row">
        <div class="col-lg-12">
          <h3 class="page-header">',($strip_html ? htmlspecialchars($title) : $title),' ',($tagline ? $strip_html ? '<small>'.htmlspecialchars($tagline).'</small>' : '<small>'.$tagline.'</small>' : ''),'</h3>
        </div>
    </div>
    ';
}
function form_file ($name) {
    $name = htmlspecialchars($name);
    $field_name = strtolower(str_replace(' ','_',$name));
    echo '<input type="file" name="',$field_name,'" id="',$field_name,'" />';
}

function form_input_text($name, $prefill = false, $disabled=false) {
    $name = htmlspecialchars($name);
    $field_name = strtolower(str_replace(' ','_',$name));
    echo '
    <div class="form-group">
      <label class="col-sm-2 control-label" for="',$field_name,'">',$name,'</label>
      <div class="col-sm-10">
          <input
            type="text"
            id="',$field_name,'"
            name="',$field_name,'"
            class="form-control"
            placeholder="',$name,'"
            ',($prefill !== false ? ' value="'.htmlspecialchars($prefill).'"' : ''),'
            ',($disabled !== false ? ' disabled' : ''),'
          />
      </div>
    </div>
    ';
}