<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 4:32 PM
 */

function layout_header()
{
    echo '
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>Challenge CTF 3</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="'.__ROOT__.'js/customjs.js" type="text/javascript"></script>
    <link rel="stylesheet" href="'.__ROOT__.'css/style3.css">
    <script type="text/javascript" src="'.__ROOT__.'soundmanager2/soundmanager2.js"></script>
	<script type="text/javascript" src="'.__ROOT__.'soundmanager2/script/bar-ui.js"></script>
	<link rel="stylesheet" href="'.__ROOT__.'soundmanager2/css/bar-ui.css">
	<link href="https://fonts.googleapis.com/css?family=Oregano:400|Open+Sans:400|Roboto+Condensed:400,600,700" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="'.__ROOT__.'">PHANTOM0305</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="'.__ROOT__.'admin">Trang chủ</a></li>
            <li><a href="'.__ROOT__.'GetSignature">Lấy chữ kí</a></li>
            <li><a href="'.__ROOT__.'admin/UploadNewSong">Tải lên</a></li>
            <li><a href="'.__ROOT__.'logout">Đăng xuất</a></li>
        </ul>
    </div>
</nav>

<div class="container"  style="margin-bottom: 50px">
    ';
}

function layout_header_user()
{
    echo '
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>Challenge CTF 3</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="'.__ROOT__.'js/customjs.js" type="text/javascript"></script>
    <link rel="stylesheet" href="'.__ROOT__.'css/style3.css">
    <script type="text/javascript" src="'.__ROOT__.'soundmanager2/soundmanager2.js"></script>
	<script type="text/javascript" src="'.__ROOT__.'soundmanager2/script/bar-ui.js"></script>
	<link rel="stylesheet" href="'.__ROOT__.'soundmanager2/css/bar-ui.css">
	<link href="https://fonts.googleapis.com/css?family=Oregano:400|Open+Sans:400|Roboto+Condensed:400,600,700" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="'.__ROOT__.'">PHANTOM0305</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="'.__ROOT__.'">Trang chủ</a></li>
            <li><a href="'.__ROOT__.'GetSignature">Lấy chữ kí</a></li>
            <li><a href="'.__ROOT__.'logout">Đăng xuất</a></li>
        </ul>
    </div>
</nav>

<div class="container"  style="margin-bottom: 50px">
    ';
}

function layout_footer()
{
    echo '
    </div>
<div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
        <p class="navbar-text pull-left">© 2018 - PHANTOM0305
        </p>

        <a href="https://www.facebook.com/manhkk035" class="navbar-btn btn-danger btn pull-right">
            <span class="glyphicon glyphicon-star"></span>  Subscribe on Facebook</a>
    </div>
</div>
</body>
</html>
    ';
}