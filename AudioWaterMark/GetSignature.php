<?php
/**
 * Created by PhpStorm.
 * User: PHANTOM0305
 * Date: 5/18/2018
 * Time: 12:31 AM
 */

include "includes/init.php";
include "includes/layout.php";
include "includes/database.php";
include "includes/message.php";
include "includes/form_layout.php";

if(!isset($_SESSION['username']))
{
    header('Location: login');
    exit();
}
if($_SESSION['isadmin']==="yes")
    layout_header();
else
    layout_header_user();
if (isset($_GET['message_error'])&&$_GET['message_error']!=='')
{
    message_dialog(htmlspecialchars($_GET['message_error']), 'LỖI', 'Ok', 'challenge-attempt incorrect on-page-load');
}
elseif(isset($_GET['message'])&&$_GET['message']!=='')
{
    message_dialog(htmlspecialchars($_GET['message']), 'Thành công', 'OK!', 'challenge-attempt correct on-page-load');
}

?>

<?php
section_subhead('Tải File Lên Để Lấy Chữ Kí');
form_start('action/GetSignature.php','','multipart/form-data');
form_file("fileGetSign");
form_hidden('action','getSign');
form_button_submit("Upload");
form_end();
?>

<?php
layout_footer();
?>