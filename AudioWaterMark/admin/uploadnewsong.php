<?php
/**
 * Created by PhpStorm.
 * User: PHANTOM0305
 * Date: 5/17/2018
 * Time: 1:07 AM
 */

include '../includes/database.php';
include '../includes/init.php';
include '../includes/layout.php';
include '../includes/session.php';
include '../includes/message.php';
include '../includes/form_layout.php';

if(!isset($_SESSION['isadmin'])||!isset($_SESSION['username'])||$_SESSION['isadmin']!='yes')
{
    header("Location: ".__ROOT__.'login');
    exit();
}
layout_header();
if (isset($_GET['message_error'])&&$_GET['message_error']!=='')
{
    message_dialog(htmlspecialchars($_GET['message_error']), 'LỖI', 'Ok', 'challenge-attempt incorrect on-page-load');
}
elseif(isset($_GET['message'])&&$_GET['message']!=='')
{
    message_dialog(htmlspecialchars($_GET['message']), 'Thành công', 'OK!', 'challenge-attempt correct on-page-load');
}
?>
<?php
    section_subhead('Upload New Song');
    form_start('admin/action/UploadNewSong.php','','multipart/form-data');
    form_input_text("Singer");
    form_input_text("Song Name");
    form_file("newSong");
    form_hidden('action','uploadNewSong');
    form_button_submit("Upload");
    form_end();
?>



<?php
layout_footer();
?>