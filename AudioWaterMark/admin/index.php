<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/21/2018
 * Time: 1:05 PM
 */
include '../includes/database.php';
include '../includes/init.php';
include '../includes/layout.php';
include '../includes/session.php';
include '../includes/message.php';

if(!isset($_SESSION['isadmin'])||!isset($_SESSION['username'])||$_SESSION['isadmin']!='yes')
{
    header("Location: ".__ROOT__.'login');
    exit();
}

function select_all_song()
{
    $query = "SELECT * FROM managerSong ORDER BY id DESC;";
    return db_query_fetch_all($query);
}

layout_header();
if (isset($_GET['message_error'])&&$_GET['message_error']!=='')
{
    message_dialog(htmlspecialchars($_GET['message_error']), 'LỖI', 'Ok', 'challenge-attempt incorrect on-page-load');
}
elseif(isset($_GET['message'])&&$_GET['message']!=='')
{
    message_dialog(htmlspecialchars($_GET['message']), 'Thành công', 'OK!', 'challenge-attempt correct on-page-load');
}
?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">QUẢN LÍ BÀI HÁT</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href=<?php echo __ROOT__.'admin';?>>QUẢN LÍ BÀI HÁT</a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!-- Bảng -->
                        <div class="section">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b>id</b>
                                            </td>
                                            <td>
                                                <b>FILE URL</b>
                                            </td>
                                            <td>
                                                <b>Tiêu đề</b>
                                            </td>
                                            <td>
                                                <b>File ID</b>
                                            </td>
                                            <td>
                                                <b>Xem/Xóa</b>
                                            </td>
                                        </tr>
                                        <?php
                                        $results = select_all_song();
                                        foreach ($results as $rows)
                                        {
                                            if($rows['name']!=='')
                                            {
                                                $content = '';
                                                $array = explode(" ",$rows['name']);
                                                if (str_word_count($rows['name']) >30)
                                                {
                                                    for($i =0;$i<30;$i++)
                                                    {
                                                        $nameSong= $nameSong.$array[$i].' ';
                                                    }
                                                    $nameSong=$nameSong.'....';
                                                }
                                                else
                                                    $nameSong = $rows['name'];
                                            }
                                            echo "
                                            <tr>
                                                <td>
                                                    <p>".htmlspecialchars($rows['id'])."</p>
                                                </td>
                                                <td>
                                                    <p>".htmlspecialchars($rows['fileUrl'])."</p>
                                                </td>
                                                <td>
                                                    <p>".htmlspecialchars($nameSong)."</p>
                                                </td>
                                                <td>
                                                    <p>".htmlspecialchars($rows['fileId'])."</p>
                                                </td>
                                                <td>
                                                    <div class=\"btn-group\">
                                                        <a class=\"btn btn-primary\" href=".__ROOT__."admin/editSong?id=".htmlspecialchars($rows["id"])."><i class=\"fa fa-fw fa-cog\"></i> Xem/Xóa</a>                                                        
                                                    </div>
                                                </td>
                                            </tr>                                        
                                            ";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
layout_footer();
?>