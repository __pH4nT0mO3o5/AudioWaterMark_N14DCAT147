<?php
/**
 * Created by PhpStorm.
 * User: PHANTOM0305
 * Date: 5/17/2018
 * Time: 12:22 PM
 */
include '../includes/database.php';
include '../includes/init.php';
include '../includes/layout.php';
include '../includes/session.php';
include '../includes/message.php';
include '../includes/form_layout.php';

if(!isset($_SESSION['isadmin'])||!isset($_SESSION['username'])||$_SESSION['isadmin']!='yes')
{
    header("Location: ".__ROOT__.'login');
    exit();
}
layout_header();

if (isset($_GET['message_error'])&&$_GET['message_error']!=='')
{
    message_dialog(htmlspecialchars($_GET['message_error']), 'LỖI', 'Ok', 'challenge-attempt incorrect on-page-load');
}
elseif(isset($_GET['message'])&&$_GET['message']!=='')
{
    message_dialog(htmlspecialchars($_GET['message']), 'Thành công', 'OK!', 'challenge-attempt correct on-page-load');
}

function select_song_by_id($id)
{
    $query ="SELECT * FROM managerSong WHERE id = :id;";
    $values = array(":id"=>$id);
    return db_query_fetch_one($query,$values);
}

if(isset($_GET['id']))
    $song = select_song_by_id($_GET['id']);
else
{
    $message = "Không có id!!!";
    header("Location: ".__ROOT__.'admin/?message_error='.$message);
    exit();
}

?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Xem Xóa Bài Hát</h1>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <p>Xem Xóa Bài Hát</p>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="col-lg-12">
                                                <p><h2><b><?php echo htmlspecialchars($song['name']);?></b></h2></p>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td><h4>URL File:</h4></td>
                                                        <td><?php echo htmlspecialchars($song['fileUrl']);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>ID:</h4></td>
                                                        <td><?php echo htmlspecialchars($song['id']);?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    section_subhead('Delete song');
                    form_start('admin/action/editSong.php');
                    form_input_checkbox('Delete confirmation');
                    form_hidden("action","deleteSong");
                    form_hidden("id",$_GET['id']);
                    form_button_submit("Delete","danger");
                    form_end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
layout_footer();
?>
