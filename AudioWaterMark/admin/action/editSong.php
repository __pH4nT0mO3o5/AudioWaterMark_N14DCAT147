<?php
/**
 * Created by PhpStorm.
 * User: PHANTOM0305
 * Date: 5/17/2018
 * Time: 12:30 PM
 */
include '../../includes/init.php';
include '../../includes/database.php';
include '../../includes/message.php';
include '../../includes/session.php';

if(!isset($_SESSION['isadmin'])||!isset($_SESSION['username'])||$_SESSION['isadmin']!='yes')
{
    header("Location: ".__ROOT__.'login');
    exit();
}

function delete_a_song($id)
{
    $query = "DELETE FROM managerSong WHERE id =:id";
    $values = array(":id"=>$id);
    return db_query_fetch_none($query,$values);
}

if($_SERVER['REQUEST_METHOD']=="POST")
{
    if(isset($_POST['action'])&&$_POST['action']==="deleteSong")
    {
        if (isset($_POST['id']))
        {
            $id = $_POST['id'];
            if(isset($_POST['delete_confirmation'])&&$_POST['delete_confirmation']==='1')
            {
                delete_a_song($id);
                $message = 'Đã xóa!!!';
                header("Location: ".__ROOT__.'admin/?message='.$message);
                exit();
            }
            else
            {
                $message_error = 'Chưa xác nhận xóa!!!';
                header("Location: ".__ROOT__.'admin/editSong?id='.$id.'&message_error='.$message_error);
                exit();
            }
        }

    }
}