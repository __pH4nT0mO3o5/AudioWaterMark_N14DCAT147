<?php
/**
 * Created by PhpStorm.
 * User: PHANTOM0305
 * Date: 5/17/2018
 * Time: 1:16 AM
 */
include '../../includes/init.php';
include '../../includes/database.php';
include '../../includes/message.php';
include '../../includes/session.php';

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

if(!isset($_SESSION['isadmin'])||!isset($_SESSION['username'])||$_SESSION['isadmin']!='yes')
{
    header("Location: ".__ROOT__.'login');
    exit();
}

function insert_song($fileUrl,$name,$fileId)
{
    $query = "INSERT INTO managerSong VALUE(NULL,:fileUrl,:name,:fileId);";
    $values = array(
        ":fileUrl"=>$fileUrl,
        ":name" => $name,
        ":fileId"=>$fileId
    );
    return db_query_fetch_none($query,$values);
}

if($_SERVER['REQUEST_METHOD']=="POST")
{
    if(isset($_POST['action'])&&$_POST['action']==="uploadNewSong")
    {
        if(isset($_FILES['newsong'])&&isset($_POST['singer'])&&$_POST['song_name'])
        {
            try
            {
                $singer = $_POST['singer'];
                $song_name = $_POST['song_name'];
                require_once '../../google-api-php-client-2.2.1/vendor/autoload.php';
                $client = new Google_Client();
                putenv('GOOGLE_APPLICATION_CREDENTIALS=../../google-api-php-client-2.2.1/service_account_keys.json');
                $client = new Google_Client();
                $client->addScope(Google_Service_Drive::DRIVE);
                $client->useApplicationDefaultCredentials();
                $service = new Google_Service_Drive($client);
                $filename = $_FILES['newsong']['tmp_name'];

                $content = file_get_contents($filename);
                $fileMetadata = new Google_Service_Drive_DriveFile(array('name' => mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $singer . " - " . $song_name . ".wav")));
                $file = $service->files->create($fileMetadata, array(
                    'data' => $content,
                    'mimeType' => 'audio/wav',
                    'uploadType' => 'multipart',
                    'fields' => 'id'));
                $fileId = $file->id;
                unlink($filename);

                $service->getClient()->setUseBatch(true);
                $batch = $service->createBatch();
                $filePermission = new Google_Service_Drive_Permission(array(
                    'type' => 'anyone',
                    'role' => 'reader',
                ));
                $request = $service->permissions->create($fileId, $filePermission, array('fields' => 'id'));
                $batch->add($request, 'anyone');
                $results = $batch->execute();
                $service->getClient()->setUseBatch(false);
                $fileUrl = "https://drive.google.com/file/d/" . $fileId . "/view?usp=sharing";
                insert_song($fileUrl,mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $singer . " - " . $song_name . ".wav"),$fileId);
                if($fileUrl)
                {
                    $message = 'Upload Thành Công!!!';
                    header("Location: ".__ROOT__.'admin/UploadNewSong?message='.$message);
                }
            }catch (Exception $e)
            {
                $message_error = "Upload Không Thành Công!!!";
                header("Location: ".__ROOT__.'admin/UploadNewSong?message_error='.$message_error);
            }

        }
    }
}