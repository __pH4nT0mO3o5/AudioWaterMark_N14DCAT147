$(document).ready(function(){
    showPageLoadModalDialogs();
    $('[data-toggle="tooltip"]').tooltip();
})

function showPageLoadModalDialogs() {
    $('.modal.on-page-load').modal();
}