<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 3/20/2018
 * Time: 4:33 PM
 */
include "includes/session.php";
include "includes/init.php";
if(!isset($_SESSION['username']))
{
    header("Location: ".__ROOT__."login");
    exit();
}
else
{
    logout();
}