-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2018 at 08:17 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chall_ctf3`
--

-- --------------------------------------------------------

--
-- Table structure for table `managerlicense`
--

CREATE TABLE `managerlicense` (
  `id` int(11) NOT NULL,
  `username` varchar(300) CHARACTER SET utf8 NOT NULL,
  `fileId` varchar(300) NOT NULL,
  `fileNewId` varchar(200) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `nameFile` varchar(300) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `managerlicense`
--

INSERT INTO `managerlicense` (`id`, `username`, `fileId`, `fileNewId`, `nameFile`) VALUES
(10, 'NguyenCongPhuong', '1AoVg_RIv4g5FnzSNxlPfr1DIolf8hGFg', '1IriJjgJN8neBxsENLLGjNa44pecfats0', 'SON TUNG MTP - Am Tham Ben Em - NguyenCongPhuong.wav');

-- --------------------------------------------------------

--
-- Table structure for table `managersong`
--

CREATE TABLE `managersong` (
  `id` int(11) NOT NULL,
  `fileUrl` varchar(300) CHARACTER SET utf8 NOT NULL,
  `name` varchar(300) CHARACTER SET utf8 NOT NULL,
  `fileId` varchar(200) CHARACTER SET ascii COLLATE ascii_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `managersong`
--

INSERT INTO `managersong` (`id`, `fileUrl`, `name`, `fileId`) VALUES
(9, 'https://drive.google.com/file/d/1fzM149H0cisxuzkCgzX427yU2Bg2ZsKy/view?usp=sharing', 'ABCC - ABCCD.wav', '1fzM149H0cisxuzkCgzX427yU2Bg2ZsKy'),
(10, 'https://drive.google.com/file/d/1AoVg_RIv4g5FnzSNxlPfr1DIolf8hGFg/view?usp=sharing', 'SON TUNG MTP - Am Tham Ben Em.wav', '1AoVg_RIv4g5FnzSNxlPfr1DIolf8hGFg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(100) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `password` varchar(100) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `isadmin` varchar(5) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `email` varchar(100) CHARACTER SET ascii COLLATE ascii_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `isadmin`, `email`) VALUES
('Manh12345', '14d20b120c10b002b7f8495db2d46350d3d1ad3c', 'no', '123@gmail.com'),
('NguyenCongPhuong', '14d20b120c10b002b7f8495db2d46350d3d1ad3c', 'no', 'NguyenCongPhuong@gmail.com'),
('admin', '218a1b57e29aacf157ea8ee01d30c21ef7060e4b', 'yes', 'letrongmanh2000@gmail.com'),
('minzy3535', '14d20b120c10b002b7f8495db2d46350d3d1ad3c', 'no', 'minzy3535@gmail.com'),
('phantom', 'c69e54ebfb625e159fbd61229963a265185f87a1', 'no', 'abc2@gmail.com'),
('phantom0305', 'c69e54ebfb625e159fbd61229963a265185f87a1', 'no', 'abc@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `managerlicense`
--
ALTER TABLE `managerlicense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managersong`
--
ALTER TABLE `managersong`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `managerlicense`
--
ALTER TABLE `managerlicense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `managersong`
--
ALTER TABLE `managersong`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
